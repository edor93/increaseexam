﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Timers;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private Timer aTimer = new Timer();
        private bool firstRun = true;
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            System.Diagnostics.Debug.WriteLine("Home Page");
            
            
            processInformation();
            return View();
        }

        private void processInformation()
        {
            Debug.WriteLine("processInformation");
            RestApiService restService = new RestApiService();
            string txtInfo = restService.getTxtFile();
            if (txtInfo != "")
            {
                ProcessTxt processTxt = new ProcessTxt();
                processTxt.process(txtInfo);
            }
            else
            {
                Debug.WriteLine("Error al obtener file");
            }

            if (firstRun)
            {
                firstRun = false;
                aTimer.Elapsed += (sender, e) => OnTimedEvent(sender, e, this);
                aTimer.Interval = 60000*10;
                aTimer.Enabled = true;
            }

        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e,HomeController hc)
        {
            Debug.WriteLine("The Elapsed event was raised at {0:HH:mm:ss.fff}",
                          e.SignalTime);
            hc.processInformation();
        }
    }
}

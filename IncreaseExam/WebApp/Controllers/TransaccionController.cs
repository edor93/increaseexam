﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class TransaccionController : ApiController
    {
        // GET api/Transaccion
        public Transaccion[] Get()
        {
            DBService dbs = new DBService();
            return dbs.getAllTransaccion();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class ClientesController : ApiController
    {
        // GET api/Clientes
        public Cliente[] Get()
        {
            DBService dbs = new DBService();
            return dbs.getAllClients();
        }


        public Cliente Get(int id)
        {
            DBService dbs = new DBService();
            return dbs.getClient(id);
        }

        [Route("api/clientes/{id}/transacciones")]
        public Transaccion[] GetClientsTransaccion(int id)
        {
            DBService dbs = new DBService();
            return dbs.getClientTransaccion(id);
        }

        [Route("api/clientes/{id}/pagos")]
        public Pago[] GetClientsPayments(int id)
        {
            DBService dbs = new DBService();
            return dbs.getClientPayments(id);
        }
    }
}

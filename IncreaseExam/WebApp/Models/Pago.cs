﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class Pago
    {
        public string id { get; set;}
        public string idExterno { get; set; }
        public string reservado { get; set;}
        public string moneda { get; set;}
        public string montoTotal { get; set;}
        public string totalDescuentos { get; set;}
        public string totalConDescuentos { get; set;}
        public string fechaPago { get; set; }
        public string idCliente { get; set; }
        public string idClienteExterno { get; set; }


        public Pago()
        {

        }

    }
}
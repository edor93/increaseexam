﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class DBService
    {
        public DBConnection dbCon { get; set; }

        public DBService()
        {

        }

        public string insertClient(Cliente cliente)
        {
            string id = "";
            string queryAdd = @"insert IGNORE into clientes " +
                "(`externalID`,`email`,`firstName`,`lastName`,`job`,`country`,`address`,`zipCode`,`phone`) " +
                "values ('" + cliente.idExterno + "','" + cliente.email + "','" + cliente.firstName + "','" + cliente.lastName + "','" + cliente.job + "','" + cliente.country.Replace("'", " ") + "','" + cliente.address.Replace("'", " ") + "','" + cliente.zipCode + "','" + cliente.phone + "') ;";

            MySqlDataReader reader = executeSQL(queryAdd);
            dbCon.Close();
            string query = "SELECT id FROM clientes WHERE externalID='" + cliente.idExterno + "'";

            reader = executeSQL(query);

            while (reader.Read())
            {
                id = reader.GetInt32(0).ToString();
                Debug.WriteLine("clienteID: " + id);
            }

            dbCon.Close();
            return id;
        }

        public string insertPago(Pago pago)
        {
            string id = "";
            string queryAdd = @"insert IGNORE into pagos " +
                "(`externalID`,`reservado`,`moneda`,`montoTotal`,`totalDescuentos`,`totalConDescuentos`,`fechaPago`,`clienteID`) " +
                "values ('" + pago.idExterno + "','" + pago.reservado + "','" + pago.moneda + "','" + pago.montoTotal + "','" + pago.totalDescuentos + "','" + pago.totalConDescuentos + "','" + pago.fechaPago + "','" + pago.idCliente + "') ;";

            MySqlDataReader reader = executeSQL(queryAdd);
            dbCon.Close();
            string query = "Select id from pagos where externalID='" + pago.idExterno + "'";

            reader = executeSQL(query);

            while (reader != null && reader.Read())
            {
                id = reader.GetString(0);
                Debug.WriteLine("pagoID: " + id);
            }
            dbCon.Close();
            return id;
        }

        public string insertTransaccion(List<Transaccion> lt)
        {
            string id = "";
            bool first = true;
            string queryAdd = @"insert IGNORE into transaccion " +
                "(`externalID`,`monto`,`reservado`,`tipo`,`pagoID`) " +
                "values ";
            foreach (var item in lt)
            {
                if (first)
                {
                    queryAdd = queryAdd + "('" + item.idExterno + "', '" + item.monto + "', '" + item.reservado + "', '" + item.tipo + "', '" + item.idPago + "')";
                    first = false;
                }
                else
                {
                    queryAdd = queryAdd + ",('" + item.idExterno + "', '" + item.monto + "', '" + item.reservado + "', '" + item.tipo + "', '" + item.idPago + "')";
                }
            }
            queryAdd = queryAdd + ";";

            MySqlDataReader reader = executeSQL(queryAdd);
            dbCon.Close();

            return id;
        }

        public string insertDescuento(List<Descuento> ld)
        {
            bool first = true;
            string id = "";
            string queryAdd = @"insert IGNORE into descuentos " +
                "(`externalID`,`monto`,`reservado`,`tipo`,`pagoID`) " +
                "values ";
            foreach (var item in ld)
            {
                if (first)
                {
                    queryAdd = queryAdd + "('" + item.idExterno + "', '" + item.monto + "', '" + item.reservado + "', '" + item.tipo + "', '" + item.idPago + "')";
                    first = false;
                }
                else
                {
                    queryAdd = queryAdd + ",('" + item.idExterno + "', '" + item.monto + "', '" + item.reservado + "', '" + item.tipo + "', '" + item.idPago + "')";
                }
            }
            queryAdd = queryAdd + ";";
            MySqlDataReader reader = executeSQL(queryAdd);
            dbCon.Close();

            return id;
        }

        public Cliente[] getAllClients()
        {
            List<Cliente> lc=new List<Cliente>();

            string query = "SELECT * FROM clientes";

            MySqlDataReader reader = executeSQL(query);

            while (reader.Read())
            {
                Cliente c = new Cliente();
                c.id = reader.GetInt32(0).ToString();
                c.idExterno = reader.GetString(1);
                c.email = reader.GetString(2);
                c.firstName = reader.GetString(3);
                c.lastName = reader.GetString(4);
                c.job = reader.GetString(5);
                c.country = reader.GetString(6);
                c.address = reader.GetString(7);
                c.zipCode = reader.GetString(8);
                c.phone = reader.GetString(9);

                lc.Add(c);
                //Debug.WriteLine("clienteID: " + id);
            }

            dbCon.Close();

            return lc.ToArray();
        }

        public Cliente getClient(int id)
        {
            Cliente c = new Cliente();

            string query = "SELECT * FROM clientes where id="+id;

            MySqlDataReader reader = executeSQL(query);

            while (reader.Read())
            {
                c.id = reader.GetInt32(0).ToString();
                c.idExterno = reader.GetString(1);
                c.email = reader.GetString(2);
                c.firstName = reader.GetString(3);
                c.lastName = reader.GetString(4);
                c.job = reader.GetString(5);
                c.country = reader.GetString(6);
                c.address = reader.GetString(7);
                c.zipCode = reader.GetString(8);
                c.phone = reader.GetString(9);

                //Debug.WriteLine("clienteID: " + id);
            }

            dbCon.Close();

            return c;
        }

        public Transaccion[] getClientTransaccion(int ClientId)
        {
            List<Transaccion> lt = new List<Transaccion>();

            string query = "SELECT t.* FROM transaccion t, pagos p where p.clienteID=" + ClientId+ " and t.pagoID=p.id";

            MySqlDataReader reader = executeSQL(query);

            while (reader.Read())
            {
                Transaccion t = new Transaccion();
                t.id = reader.GetInt32(0).ToString();
                t.idExterno = reader.GetString(1);
                t.monto = reader.GetInt32(2).ToString();
                t.reservado = reader.GetString(3);
                t.tipo = reader.GetString(4);
                t.idPago = reader.GetString(5);

                lt.Add(t);
                //Debug.WriteLine("clienteID: " + id);
            }

            dbCon.Close();

            return lt.ToArray();
        }

        public Pago[] getClientPayments(int ClientId)
        {
            List<Pago> lp = new List<Pago>();

            string query = "SELECT * FROM pagos where clienteID=" + ClientId;

            MySqlDataReader reader = executeSQL(query);

            while (reader.Read())
            {
                Pago p = new Pago();
                p.id = reader.GetInt32(0).ToString();
                p.idExterno = reader.GetString(1);
                p.reservado = reader.GetString(2);
                p.moneda = reader.GetString(3);
                p.montoTotal = reader.GetInt32(4).ToString();
                p.totalDescuentos = reader.GetInt32(5).ToString();
                p.totalConDescuentos = reader.GetInt32(6).ToString();
                p.fechaPago = reader.GetDateTime(7).ToString();
                p.idCliente = reader.GetInt32(8).ToString();
                p.idClienteExterno = "";
                lp.Add(p);
                //Debug.WriteLine("clienteID: " + id);
            }

            dbCon.Close();

            return lp.ToArray();
        }

        public Transaccion[] getAllTransaccion()
        {
            List<Transaccion> lt = new List<Transaccion>();

            string query = "SELECT * FROM transaccion";

            MySqlDataReader reader = executeSQL(query);

            while (reader.Read())
            {
                Transaccion t = new Transaccion();
                t.id = reader.GetInt32(0).ToString();
                t.idExterno = reader.GetString(1);
                t.monto = reader.GetInt32(2).ToString();
                t.reservado = reader.GetString(3);
                t.tipo = reader.GetString(4);
                t.idPago = reader.GetString(5);

                lt.Add(t);
                //Debug.WriteLine("clienteID: " + id);
            }

            dbCon.Close();

            return lt.ToArray();
        }

        private MySqlDataReader executeSQL(string queryAdd)
        {
            dbCon = DBConnection.Instance();
            MySqlDataReader myReader = null;

            if (dbCon.IsConnect())
            {
                MySqlCommand cmdDataBase = new MySqlCommand(queryAdd, dbCon.Connection);

                try
                {
                    myReader = cmdDataBase.ExecuteReader();
                    Debug.WriteLine("Saved");
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }

            return myReader;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class Cliente
    {
        public string id { get; set; }
        public string idExterno { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string job { get; set; }
        public string country { get; set; }
        public string address { get; set; }
        public string zipCode { get; set; }
        public string phone { get; set; }

        public Cliente()
        {

        }
    }
}
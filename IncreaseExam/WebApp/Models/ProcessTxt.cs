﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class ProcessTxt
    {
        public ProcessTxt()
        {

        }

        public void process(string txt)
        {
            Debug.WriteLine("process");

            string[] lines = txt.Split(
                new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.None
            );
            var listTran = new List<Transaccion>();
            var listDesc = new List<Descuento>();
            Pago c = new Pago();
            bool isSaveDB = false;

            foreach (var line in lines)
            {
                char[] lineChars = line.ToCharArray();
                switch (line[0])
                {

                    case '1':
                        c.idExterno = subArray(lineChars,1, 32);
                        c.reservado = subArray(lineChars, 33, 3);
                        if(subArray(lineChars, 36, 3) == "000")
                        {
                            c.moneda = "ARS";
                        }
                        else
                        {
                            c.moneda = "USD";
                        }
                        c.montoTotal = subArray(lineChars, 39, 13);
                        c.totalDescuentos = subArray(lineChars, 52, 13);
                        c.totalConDescuentos = subArray(lineChars, 65, 13);
                        Debug.WriteLine("It is 1");
                        Debug.WriteLine(c.idExterno+"|"+c.reservado+"|"+c.moneda+"|"+c.montoTotal+"|"+c.totalDescuentos+"|"+c.totalConDescuentos);

                        break;

                    case '2':
                        Transaccion t = new Transaccion();
                        t.idExterno = subArray(lineChars, 1, 32);
                        t.monto = subArray(lineChars, 33, 13);
                        t.reservado = subArray(lineChars, 46, 5);
                        if(subArray(lineChars, 51, 1) == "1")
                        {
                            t.tipo = "Aprobado";
                        }
                        else
                        {
                            t.tipo = "Rechazado";
                        }
                        listTran.Add(t);
                        Debug.WriteLine("It is 2");
                        Debug.WriteLine(t.idExterno + "|" + t.monto + "|" + t.reservado + "|" + t.tipo);
                        break;

                    case '3':
                        Descuento d = new Descuento();
                        d.idExterno = subArray(lineChars, 1, 32);
                        d.monto = subArray(lineChars, 33, 13);
                        d.reservado = subArray(lineChars, 46, 3);
                        switch (subArray(lineChars, 49, 1))
                        {
                            case "0":
                                d.tipo = "IVA";
                                break;
                            case "1":
                                d.tipo = "Retenciones";
                                break;
                            case "2":
                                d.tipo = "Comisiones";
                                break;
                            case "3":
                                d.tipo = "Costos extra";
                                break;
                            case "4":
                                d.tipo = "Ingresos brutos";
                                break;
                            default:
                                d.tipo = "";
                                break;
                        }
                        listDesc.Add(d);
                        Debug.WriteLine("It is 3");
                        Debug.WriteLine(d.idExterno + "|" + d.monto + "|" + d.reservado + "|" + d.tipo);
                        break;

                    case '4':
                        var date = DateTime.ParseExact(subArray(lineChars, 16, 8), "yyyyMMdd",CultureInfo.InvariantCulture);
                        c.fechaPago = date.ToString("yyyy-MM-dd H:mm:ss"); 
                        c.idClienteExterno = subArray(lineChars, 24, 32);
                        isSaveDB = true;
                        Debug.WriteLine("It is 4");
                        Debug.WriteLine(c.fechaPago + "|" + c.idClienteExterno);
                        break;

                    default:
                        Debug.WriteLine("Nothing");
                        break;
                }

                if (isSaveDB)
                {
                    saveDB(c,listTran,listDesc);
                    c = new Pago();
                    listTran = new List<Transaccion>();
                    listDesc = new List<Descuento>();
                    isSaveDB = false;
                    
                }
               
            }
            
            //Debug.WriteLine(lines[0]);


        }


        public static string subArray(char[] data, int index, int length)
        {
            char[] result = new char[length];
            Array.Copy(data, index, result, 0, length);
            return new string(result);
        }


        private void saveDB(Pago c,List<Transaccion> lt,List<Descuento> ld)
        {
            RestApiService restApi = new RestApiService();
            Cliente cli = restApi.getClient(c.idClienteExterno);
            if (cli != null)
            {
                DBService dbs = new DBService();
                string idCliente = dbs.insertClient(cli);
                c.idCliente = idCliente;
                string idPago = dbs.insertPago(c);

                foreach (var item in lt)
                {
                    item.idPago = idPago;
                }
                dbs.insertTransaccion(lt);
                foreach (var item in ld)
                {
                    item.idPago = idPago;
                }
                dbs.insertDescuento(ld);
                Debug.WriteLine("inserts DONE...");
            }
            else
            {
                Debug.WriteLine("Error al obtener cliente");
            }
           
        }
    }
}
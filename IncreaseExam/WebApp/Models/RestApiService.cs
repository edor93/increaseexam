﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace WebApp.Models
{
    public class RestApiService
    {
        public RestApiService(){
            
        }
        public string getTxtFile()
        {
            return getRequest("https://increase-transactions.herokuapp.com/file.txt");
        }

        public Cliente getClient(string id)
        {
            Debug.WriteLine("getClient");
            string content = getRequest("https://increase-transactions.herokuapp.com/clients/"+id);
            Cliente c = new Cliente();
            if (content != "")
            {
                JObject joResponse = JObject.Parse(content);

                c.idExterno = joResponse["id"].ToString();
                c.email = joResponse["email"].ToString();
                c.firstName = joResponse["first_name"].ToString();
                c.lastName = joResponse["last_name"].ToString();
                c.job = joResponse["job"].ToString();
                c.country = joResponse["country"].ToString();
                c.address = joResponse["address"].ToString();
                c.zipCode = joResponse["zip_code"].ToString();
                c.phone = joResponse["phone"].ToString();

                Debug.WriteLine(c.idExterno + "|" + c.firstName + "|" + c.lastName + "|" + c.job + "|" + c.country + "|" + c.address + "|" + c.zipCode + "|" + c.phone);
            }
            else
            {
                c = null;
            }

            return c;
            
        }

        private string getRequest(string url)
        {
            var content = "";
            var http = (HttpWebRequest)WebRequest.Create(new Uri(url));
            //http.Accept = "application/json";
            //http.ContentType = "application/json";
            http.Method = "GET";
            string authorization = "Bearer 1234567890qwertyuiopasdfghjklzxcvbnm";
            http.Headers["Authorization"] = authorization;

            ASCIIEncoding encoding = new ASCIIEncoding();
            try
            {
                var response = http.GetResponse() as HttpWebResponse;
                if (response.StatusCode == HttpStatusCode.OK)
                {

                    var stream = response.GetResponseStream();
                    var sr = new StreamReader(stream, Encoding.UTF8);
                    content = sr.ReadToEnd();

                    //Debug.WriteLine(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            

            return content;
        }
    }
}
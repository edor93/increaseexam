﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Models
{
    public class Descuento
    {
        public string id { get; set; }
        public string idExterno { get; set; }
        public string monto { get; set; }
        public string reservado { get; set; }
        public string tipo { get; set; }
        public string idPago { get; set; }
        public Descuento()
        {

        }
    }
}